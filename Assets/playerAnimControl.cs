﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerAnimControl : MonoBehaviour
{
    public Animator thisAnim;
    public ParticleSystem slapParticle;
    public ParticleSystem FireFistParticle;

    public bool sureKill;
    public enum Type
    {
        hero,enemy,npc
    }

    public Type type;
    // Start is called before the first frame update
    void Start()
    {
        //thisAnim = GetComponentInChildren<Animator>();
        IdleState();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IdleState()
    {
        if(type == Type.hero)
        {
            thisAnim.SetTrigger("happyIdle");
        }
        else if(type == Type.enemy)
        {
            thisAnim.SetTrigger("idle");
        }
        
    }
    
    public void StartShaking()
    {
        thisAnim.SetTrigger("shaking");
    }

    public void Paper()
    {
        thisAnim.SetTrigger("paper");
    }
    public void Scissor()
    {
        thisAnim.SetTrigger("scissor");
    }
    public void Rock()
    {
        thisAnim.SetTrigger("rock");
    }

    public void Win()
    {
        thisAnim.SetTrigger("win");
    }

    public void Loose()
    {
        thisAnim.SetTrigger("loose");
    }

    public void Draw()
    {
        thisAnim.SetTrigger("draw");
    }

    public void Slap()
    {
        thisAnim.SetTrigger("slap");

        if(type == Type.hero)
        {
            if (GameStatesControl.instance.SureDeathAnticipation(0))
            {
                FireFistParticle.Play();
                Invoke("FireFistParticleOff", 1f);
            }


        }
        else if (type == Type.enemy)
        {
            if (GameStatesControl.instance.SureDeathAnticipation(1))
            {
                FireFistParticle.Play();
                Invoke("FireFistParticleOff", 1f);
            }
        }
        //sureKill = GameStatesControl.instance.SureDeathAnticipation(0);
        
        //FireFistParticle.Play();
        //Invoke("FireFistParticleOff", 1f);
    }
    public void GetSlap()
    {
        thisAnim.SetTrigger("getSlap");

        Invoke("GetDamage", 0.35f);
    }

    public void GetSlapDelayed(float d)
    {
        Invoke("GetSlap", d);
    }

    public void GetDamage()
    {
        if (type == Type.hero)
        {
            PlayersLifeMeterControl.instance.DamageHero();
            LifeBoardPanel.instance.UpdateLifeBoard();
        }
        else if (type == Type.enemy)
        {
            PlayersLifeMeterControl.instance.DamageEnemy();
            LifeBoardPanel.instance.UpdateLifeBoard();
        }
        if (slapParticle)
        {
            slapParticle.Play();
        }
        
    }

    public void FireFistParticleOff()
    {
        FireFistParticle.Stop();
    }
}
