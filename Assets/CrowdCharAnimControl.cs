﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrowdCharAnimControl : MonoBehaviour
{
    public int AnimTypes;
    Animator CrownAnim;

    public GameObject[] croudChars;
    public int charAliveIndex;

    public bool GetRandom;
    // Start is called before the first frame update
    void Start()
    {
        if (GetRandom)
        {
            charAliveIndex = Random.Range(0, 3);
        }
        
        for (int i = 0; i < croudChars.Length; i++)
        {
            croudChars[i].SetActive(false);

            if(i == charAliveIndex)
            {
                croudChars[i].SetActive(true);
            }
        }
        
        CrownAnim = croudChars[charAliveIndex].GetComponentInChildren<Animator>();
        ChangeAnim();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void ChangeAnim()
    {
        int i = Random.Range(0, AnimTypes);
        CrownAnim.SetInteger("cheer", i);

        float f = Random.Range(4, 8);

        Invoke("ChangeAnim", f);

    }
}
