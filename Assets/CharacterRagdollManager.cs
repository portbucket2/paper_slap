﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterRagdollManager : MonoBehaviour
{
    public Collider[] ragColliders;
    public Animator CharAnim;
    public Rigidbody chestRb;

    public bool RagdollMode;
    // Start is called before the first frame update
    void Start()
    {
        DisableRagDoll();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SlapForceToRagdoll();
            //chestRb.AddForce(0, 20000, 8000);
            //chestRb.AddForce(30000 * transform.right + -8000*transform.forward + 20000* transform.up);
        }
        if (Input.GetKeyDown(KeyCode.T))
        {
            DisableRagDoll();
            //chestRb.AddForce(-30000, 20000, 8000);
        }
    }

    public void SlapForceToRagdoll()
    {
        EnableRagDoll();

        if( this.GetComponentInParent<playerAnimControl>().type == playerAnimControl.Type.hero)
        {
            chestRb.AddForce(8000 * transform.right + -2000 * transform.forward + 5000 * transform.up);
        }
        else if (this.GetComponentInParent<playerAnimControl>().type == playerAnimControl.Type.enemy)
        {
            chestRb.AddForce(30000 * transform.right + -8000 * transform.forward + 20000 * transform.up);
        }
        //chestRb.AddForce(30000 * transform.right + -8000 * transform.forward + 20000 * transform.up);
    }

    public void EnableRagDoll()
    {
        for (int i = 0; i < ragColliders.Length; i++)
        {
            ragColliders[i].enabled = true;
            ragColliders[i].gameObject.GetComponent<Rigidbody>().useGravity = true;
        }

        CharAnim.enabled = false;
        RagdollMode = true;

    }

    public void DisableRagDoll()
    {
        for (int i = 0; i < ragColliders.Length; i++)
        {
            ragColliders[i].enabled = false;
            ragColliders[i].gameObject.GetComponent<Rigidbody>().useGravity = false;
        }
        CharAnim.enabled = true;
        RagdollMode = false;
    }
}
