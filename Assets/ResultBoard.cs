﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultBoard : MonoBehaviour
{
    public GameObject leftImage;
    public GameObject rightImage;

    public Transform leftPos;
    public Transform rightPos;
    public Transform leftOutPos;
    public Transform rightOutPos;
    public Transform CenterPos;
    public Transform UpPos;

    public Vector3 leftImagePos;
    public Vector3 rightImagePos;

    public Text LeftText;
    public Text RightText;

    public float leftScale;
    public float rightScale;

    public float rightFill;
    public float leftFill;

    public static ResultBoard instance;

    public Sprite[] imagePSR;
    public Image ScreenLeft;
    public Image ScreenRight;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        leftImagePos = leftImage.transform.position;
        rightImagePos = rightImage.transform.position;
        leftImage.transform.position = leftImagePos = leftOutPos.position;
        rightImage.transform.position = rightImagePos = rightOutPos.position;

        leftScale = rightScale = 1;
    }

    // Update is called once per frame
    void Update()
    {
        leftImage.transform.position = Vector3.Lerp(leftImage.transform.position, leftImagePos, Time.deltaTime * 5);
        rightImage.transform.position = Vector3.Lerp(rightImage.transform.position, rightImagePos, Time.deltaTime * 5);

        leftImage.transform.localScale = Vector3.Lerp(leftImage.transform.localScale, Vector3.one * leftScale, Time.deltaTime * 5);
        rightImage.transform.localScale = Vector3.Lerp(rightImage.transform.localScale, Vector3.one * rightScale, Time.deltaTime * 5);

        rightImage.GetComponent<Image>().fillAmount = Mathf.MoveTowards(rightImage.GetComponent<Image>().fillAmount , rightFill , Time.deltaTime * 1);
        leftImage.GetComponent<Image>().fillAmount = Mathf.MoveTowards(leftImage.GetComponent<Image>().fillAmount, leftFill, Time.deltaTime * 1);


    }

    public void GetSigns(int a , int b)
    {
        
        if (a == 0)
        {
            LeftText.text = "Paper";
            ScreenLeft.sprite = imagePSR[a];
        }
        else if(a == 1)
        {
            LeftText.text = "Scissor";
        }
        else if(a== 2)
        {
            LeftText.text = "Rock";
        }
        if (b== 0)
        {
            RightText.text = "Paper";
        }
        else if (b == 1)
        {
            RightText.text = "Scissor";
        }
        else if (b == 2)
        {
            RightText.text = "Rock";
        }

        ScreenLeft.sprite = imagePSR[a];
        ScreenRight.sprite = imagePSR[b];

        GetOnScreen();
    }

    public void GetOnScreen()
    {
        leftImagePos = leftPos.position;
        rightImagePos = rightPos.position;

        leftScale = rightScale = 1;
        rightFill = leftFill = 0;
    }

    public void LeaveTheLostlost(int i)
    {
        if (i == 0)
        {
            leftImagePos = CenterPos.position;
            rightImagePos = rightOutPos.position;

            leftScale = rightScale = 1.5f;
            leftFill = 1;
            rightFill = 0;

            GameManagement.instance.youWinParticle.Play();
        }
        else if (i== 1)
        {
            leftImagePos = leftOutPos.position;
            rightImagePos = CenterPos.position;

            leftScale = rightScale = 1.5f;
            rightFill = 1;
            leftFill = 0;
        }
        else if (i == 2)
        {
            leftImagePos = leftOutPos.position;
            rightImagePos = rightOutPos.position;

            leftScale = rightScale = 1;
        }
    }
    public void LeveAll()
    {
        leftImagePos = leftOutPos.position;
        rightImagePos = rightOutPos.position;

        leftScale = rightScale = 1;
        rightFill = leftFill = 0;
    }
}
