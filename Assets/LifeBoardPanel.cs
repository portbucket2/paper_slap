﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeBoardPanel : MonoBehaviour
{
    public Text HeroLifeText;
    public Text EnemyLifeText;

    public Slider HeroLifeSlider;
    public Slider EnemyLifeSlider;

    public static LifeBoardPanel instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    public void UpdateLifeBoard()
    {
        HeroLifeText.text = ""+PlayersLifeMeterControl.instance.HeroLife;
        EnemyLifeText.text = "" + PlayersLifeMeterControl.instance.EnemyLife;

        float h = PlayersLifeMeterControl.instance.HeroLife / PlayersLifeMeterControl.instance.HeroLifeMax;
        float e = PlayersLifeMeterControl.instance.EnemyLife / PlayersLifeMeterControl.instance.EnemyLifeMax;

        HeroLifeSlider.value = h;
        EnemyLifeSlider.value = e;
    }


}
