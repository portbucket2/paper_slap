﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagement : MonoBehaviour
{
    public static GameManagement instance;
    public enum State
    {
        idle , shaking , signed , result, meter , action , postEffect, knockout
    }
    public State state;

    public ParticleSystem youWinParticle;
    public ParticleSystem youWinFinallyParticle;

    //public bool KnockedOut;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        ChangeStateTo(0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeStateTo(int i)
    {
        state = (State)i;
        UiManage.instance.GetPanel(i);
    }
}
