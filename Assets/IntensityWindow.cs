﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IntensityWindow : MonoBehaviour
{
    public float angle;
    public bool IntensityMeter;
    public float value;
    public float speed;

    //public GameObject pointer;

    public GameObject boardHolder;

    public GameObject boardUiHolder;
    public GameObject MeshPointer;

    public float lastIntensity;

    public static IntensityWindow instance;

    public Text intensityDamageText;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        boardHolder.SetActive(false);
        boardUiHolder.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        IntensityMeterFunction();
    }

    public void IntensityMeterFunction()
    {
        if (IntensityMeter)
        {
            angle += Time.deltaTime *speed;

            value = Mathf.Sin(angle );

            //pointer.transform.localPosition = new Vector3(value * 350f, 0, 0);

            MeshPointer.transform.localRotation = Quaternion.Euler(new Vector3(0, 0,value * 90));

            intensityDamageText.text = "" + PlayersLifeMeterControl.instance.HeroHitPower;
        }
        //else
        //{
        //    angle = 0;
        //    value = 0;
        //    pointer.transform.localPosition = new Vector3(0, 0, 0);
        //}
    }

    public void EnableIntensityMeter(int i)
    {
        if(i == 0)
        {
            boardHolder.SetActive(true);
            boardUiHolder.SetActive(true);
            IntensityMeter = true;
        }
        else
        {
            HideIntesityBoardHolder();
        }
        //IntensityMeter = true;
    }
    public void DisableIntensityMeter()
    {
        lastIntensity =0.5f + (0.5f*( 1 - Mathf.Abs(value)));

        IntensityMeter = false;
        angle = 0;
        value = 0;
        //pointer.transform.localPosition = new Vector3(0, 0, 0);

        boardUiHolder.SetActive(false);

        intensityDamageText.text = "" + (int)(PlayersLifeMeterControl.instance.HeroHitPower*lastIntensity);
        Invoke("HideIntesityBoardHolder", 1.5f);


    }


    void HideIntesityBoardHolder()
    {
        boardHolder.SetActive(false);
        boardUiHolder.SetActive(false);
        MeshPointer.transform.localRotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }

    
}
