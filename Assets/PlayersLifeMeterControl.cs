﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayersLifeMeterControl : MonoBehaviour
{
    public float HeroLife;
    public float EnemyLife;

    public float HeroLifeMax;
    public float EnemyLifeMax;

    public float HeroHitPower;
    public float EnemyHitPower;

    public float intensityOfDamage;

    public bool KnockedOut;

   

    public static PlayersLifeMeterControl instance;

    private void Awake()
    {
        instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {
        HeroLife = HeroLifeMax;
        EnemyLife = EnemyLifeMax;
        LifeBoardPanel.instance.UpdateLifeBoard();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void HeroKnockOut()
    {
        GameStatesControl.instance.playerAnim.GetComponentInChildren<CharacterRagdollManager>().SlapForceToRagdoll();
        //GameManagement.instance.KnockedOut = true;
        KnockedOut = true;
        //GameStatesControl.instance.KnockOutState();
    }
    public void EnemyKnockOut()
    {
        GameStatesControl.instance.OpponentAnim.GetComponentInChildren<CharacterRagdollManager>().SlapForceToRagdoll();
        //GameManagement.instance.KnockedOut = true;
        KnockedOut = true;
        //GameStatesControl.instance.KnockOutState();
    }

    public void DamageHero()
    {
        //intensityOfDamage = Random.Range(0.5f, 1);
        intensityOfDamage = 1;
        HeroLife -= EnemyHitPower * intensityOfDamage;
        HeroLife = (int)HeroLife;
        if (HeroLife <= 0)
        {
            HeroLife = 0;
            HeroKnockOut();
        }
    }
    public void DamageEnemy()
    {
        intensityOfDamage = IntensityWindow.instance.lastIntensity;
        
        EnemyLife -= HeroHitPower * intensityOfDamage;
        EnemyLife = (int)EnemyLife;
        if(EnemyLife <= 0)
        {
            EnemyLife = 0;
            EnemyKnockOut();
        }
    }

    public void ResetLivesHeroAndEnemy()
    {
        HeroLife = HeroLifeMax;
        EnemyLife = EnemyLifeMax;
        LifeBoardPanel.instance.UpdateLifeBoard();
        KnockedOut = false;
    }
}
