﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraHolder : MonoBehaviour
{
    public GameObject cameraParent;
    Vector3 camPos;
    Quaternion camRot;

    public Transform midPos;
    public Transform winPos;
    public Transform loosePos;

    public static CameraHolder instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        CameraMidPos();
    }

    // Update is called once per frame
    void Update()
    {
        cameraParent.transform.position = Vector3.Lerp(cameraParent.transform.position, camPos, Time.deltaTime * 5);
        cameraParent.transform.rotation = Quaternion.Lerp(cameraParent.transform.rotation, camRot, Time.deltaTime * 5);
    }

    public void CameraMidPos()
    {
        camPos = midPos.position;
        camRot = midPos.rotation;
    }

    public void CameraWinPos()
    {
        camPos = winPos.position;
        camRot = winPos.rotation;
    }

    public void CameraLoosePos()
    {
        camPos = loosePos.position;
        camRot = loosePos.rotation;
    }

    public void GetCameraPos(int c)
    {
        if (c == 0)
        {
            CameraWinPos();
        }
        else if (c == 1)
        {
            CameraLoosePos();
        }
        else
        {
            CameraMidPos();
        }
    }
}
