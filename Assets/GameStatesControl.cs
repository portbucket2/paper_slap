﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStatesControl : MonoBehaviour
{

    public playerAnimControl playerAnim;
    public playerAnimControl OpponentAnim;

    //public int j;

    public int playersCall;
    public int enemysCall;

    public int winLooseDraw;

    public static GameStatesControl instance;

    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartShaking()
    {
        CameraHolder.instance.GetCameraPos(0);
        GameManagement.instance.ChangeStateTo(1);
        playerAnim.GetComponentInChildren<CharacterRagdollManager>().DisableRagDoll();
        OpponentAnim.GetComponentInChildren<CharacterRagdollManager>().DisableRagDoll();

        

        playerAnim.StartShaking();
        OpponentAnim.StartShaking();
        

    }

    public void PaperSelect()
    {
        playersCall = 0;
        GameManagement.instance.ChangeStateTo(2);
        UiManage.instance.ShowSign(0);
        playerAnim.Paper();

        Invoke("GetResult",1.2f);

        //Invoke("EnemySigning", 0.1f);
        EnemySigning();

        CameraHolder.instance.GetCameraPos(2);
    }
    public void ScissorSelect()
    {
        playersCall = 1;
        GameManagement.instance.ChangeStateTo(2);
        UiManage.instance.ShowSign(1);
        playerAnim.Scissor();

        Invoke("GetResult",1.2f);

        //Invoke("EnemySigning", 0.1f);
        EnemySigning();

        CameraHolder.instance.GetCameraPos(2);
    }
    public void RockSelect()
    {
        playersCall = 2;
        GameManagement.instance.ChangeStateTo(2);
        UiManage.instance.ShowSign(2);
        playerAnim.Rock();

        Invoke("GetResult", 1.2f);

        //Invoke("EnemySigning", 0.1f);
        EnemySigning();

        CameraHolder.instance.GetCameraPos(2);
    }

    public void EnemySigning()
    {
        int j = Random.Range(0, 3);
        enemysCall = j;
        if (j == 0)
        {
            OpponentAnim.Paper();
        }
        else if (j == 1)
        {
            OpponentAnim.Scissor();

        }
        else if (j == 2)
        {
            OpponentAnim.Rock();
        }

        ResultBoard.instance.GetSigns(playersCall, enemysCall);
    }


    public void GetResult()
    {
        

        JudgementCall(CalculateResult());
        ResultBoard.instance.LeaveTheLostlost(winLooseDraw);
    }
    public void JudgementCall( int j)
    {
        //j = Random.Range(0, 3);
        //j = 2;

        if(j == 0)
        {
            playerAnim.Win();
            OpponentAnim.Loose();

            GameManagement.instance.ChangeStateTo(3);
            UiManage.instance.WinOrLooseText.text = "You Won !!!";
            Invoke("GetMeterIntensity", 2f);
        }
        else if(j == 1)
        {
            playerAnim.Loose();
            OpponentAnim.Win();

            GameManagement.instance.ChangeStateTo(3);
            UiManage.instance.WinOrLooseText.text = "You Lost !!!";
            Invoke("GetMeterIntensity",2f);

        }
        else if(j == 2)
        {
            playerAnim.Draw();
            OpponentAnim.Draw();

            GameManagement.instance.ChangeStateTo(3);
            UiManage.instance.WinOrLooseText.text = "DRAW !!!";
            //GameManagement.instance.ChangeStateTo(1);
            Invoke("ResetToShaking", 1.2f);
            //Invoke("GetMeterIntensity", 1f);
        }
        
    }

    public void GetMeterIntensity()
    {
        CameraHolder.instance.GetCameraPos(winLooseDraw);
        
        GameManagement.instance.ChangeStateTo(4);
        IntensityWindow.instance.EnableIntensityMeter(winLooseDraw);

        if(winLooseDraw == 1)
        {
            Invoke("Action", 0.5f);
        }

        ResultBoard.instance.LeveAll();
    }

    public void Action()
    {
        int j = winLooseDraw;
        GameManagement.instance.ChangeStateTo(5);
        
        IntensityWindow.instance.DisableIntensityMeter();
        if (j == 0)
        {
            
            playerAnim.Slap();
            OpponentAnim.GetSlapDelayed(0.5f);

        }
        else if (j == 1)
        {

            playerAnim.GetSlapDelayed(0.5f);
            OpponentAnim.Slap();
        }
        else if (j == 2)
        {
            //playerAnim.Draw();
        }

        Invoke("PostEffects", 2f);
    }

    public void PostEffects()
    {
        GameManagement.instance.ChangeStateTo(6);
        UiManage.instance.GetPostFxText(winLooseDraw);

        if (PlayersLifeMeterControl.instance.KnockedOut)
        {
            KnockOutState();
            if (PlayersLifeMeterControl.instance.EnemyLife <= 0)
            {
                GameManagement.instance.youWinFinallyParticle.Play();
                UiManage.instance.KnockedOutText.text = "JUMBO";
            }
            else
            {
                
                UiManage.instance.KnockedOutText.text = "YOU are";
            }
        }
    }

    public void KnockOutState()
    {
        GameManagement.instance.ChangeStateTo(7);
    }

    public void ResetToShaking()
    {
        

        if (GameManagement.instance.state == GameManagement.State.knockout)
        {
            PlayersLifeMeterControl.instance.ResetLivesHeroAndEnemy();

            
            //GameManagement.instance.KnockedOut = false;
        }
        StartShaking();
        //CameraHolder.instance.GetCameraPos(2);
    }


    public int CalculateResult()
    {
        if(playersCall > enemysCall)
        {
            winLooseDraw = 0;
        }
        else if(playersCall < enemysCall)
        {
            winLooseDraw = 1;
        }
        else
        {
            winLooseDraw = 2;
        }

        if(playersCall == 2 && enemysCall == 0)
        {
            winLooseDraw = 1;
        }
        else if (playersCall == 0 && enemysCall == 2)
        {
            winLooseDraw = 0;
        }

        return winLooseDraw;
    }

    public bool SureDeathAnticipation(int i)
    {
        if(i == 0)
        {
            if(PlayersLifeMeterControl.instance.HeroHitPower * IntensityWindow.instance.lastIntensity >= PlayersLifeMeterControl.instance.EnemyLife)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else if(i == 1)
        {
            if (PlayersLifeMeterControl.instance.EnemyHitPower * 1 >= PlayersLifeMeterControl.instance.HeroLife)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }
    
}
