﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UiManage : MonoBehaviour
{
    public static UiManage instance;

    public GameObject[] panels;

    public Text signText;
    public Text WinOrLooseText;
    public Text PostFxText;
    public Text KnockedOutText;
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void GetPanel(int i)
    {
        CloseAllPanels();
        panels[i].SetActive(true);
    }

    public void CloseAllPanels()
    {
        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(false);
        }
    }

    public void ShowSign(int j)
    {
        if (j == 0)
        {
            signText.text = "PAPER";
        }
        else if (j == 1)
        {
            signText.text = "SCISSOR";
        }
        else if (j == 2)
        {
            signText.text = "ROCK";
        }
    }

    public void GetPostFxText(int p)
    {
        if (p == 0)
        {
            PostFxText.text = "YOU ARE THE BOSS !!!";
        }
        else if (p == 1)
        {
            PostFxText.text = "SHAME...!!";
        }
        else
        {
            PostFxText.text = "Draw";
        }
    }
}
